function checkSum = checksum(segment, checksumType)
    switch checksumType
        case 'Parzystość'
            checkSum = mod(nnz(segment),2);
        case 'CRC'
            
            message = [segment;zeros(32,1)];
            poly = [1;0;0;0;0;0;1;0;0;1;1;0;0;0;0;0;1;0;0;0;1;1;1;0;1;1;0;1;1;0;1;1;1];
            while (size(message,1)>32)
                if(message(1:1)==0)
                    message=message(2:end);
                else
                    message=[xor(message(1:33),poly);message(34:end)];
                end
            end
            %disp(reshape(message,[1,32]));
            checkSum=message;

        otherwise
            checkSum = int16.empty;
    end
end