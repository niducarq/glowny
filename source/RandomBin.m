%retRandomBin (n)
% Funckja ta zwraca kolumne losowo wygenerowanych liczb 0 lub 1.
% Korzystaj�c z funkcji rand, generowana jest kolumna losowego ci�gu liczb 
% z przedzia�u [0,1], po czym dana liczba jest zaokr�glana w g�re lub w d�.
% n jest argumentem s�u��cym do zdefiniowania ilo�ci tych liczb.

function x = RandomBin(n)
x = rand(n,1);

for idx = 1:n
    x(idx,1) = round(x(idx,1));
end
end