classdef GoBackN
    properties
        receivedData;
        retransmitions;
        sentPackets;
        
        data;
        packetLen;
        checksumType;
        channelType;
        
        errg;
        errb;
        gtb;
        btg;
    end
    methods
        function obj = GoBackN(data,packetLen, checksumType,channelType,errg,errb,btg,gtb)
           obj.receivedData=int16.empty;
           obj.retransmitions=0;
           obj.sentPackets=0;
           obj.data=data;
           obj.packetLen=packetLen;
           obj.checksumType=checksumType;
           obj.channelType=channelType;
           obj.errg=errg;
           obj.errb=errb;
           obj.gtb=gtb;
           obj.btg=btg;
        end
        
        function war = getretransmitions(obj)
            war = obj.retransmitions;
        end
        
        function war = getsentPackets(obj)
            war = obj.sentPackets;
        end
        
        function war = getdata(obj)
            war = obj.data;
        end
        
        function war = getreceivedData(obj)
            war = obj.receivedData;
        end
        
        function obj = GoBackN_run(obj)
            % Funkcja przeprowadzaj�ca symulacj� ARQ typu Stop And Wait.
            %   Argumenty:
            %       data: dane do przes�ania
            %       packetLen: d�ugo�� pakietu
            %       checksumType: typ kontroli poprawno�ci
            %       channelType: typ kana�u
            %   Wynik:
            %       receivedData: dane odebrane
            %       retransmissions: przeprowadzone retransmisje
            if(mod(length(obj.data),obj.packetLen)~=0)
                message= [obj.data; zeros(obj.packetLen-mod(length(obj.data),obj.packetLen),1)];
            else
                message=obj.data;
            end

            segments=reshape(message, obj.packetLen,[]);
            
            i=1;
            window=5;
            while i <= size(segments,2)
                sent=[segments(:,i);checksum(segments(:,i), obj.checksumType)];
                canal = TransmissionChannel(sent,obj.errg,obj.errb,obj.gtb,obj.btg);
                received=GenerateNoise(canal);
                obj.sentPackets=obj.sentPackets+1;
                if (~isequal(received(obj.packetLen+1:end),checksum(received(1:obj.packetLen), obj.checksumType)))
                    obj.retransmitions=obj.retransmitions+window;
                    window=5;
                else
                    obj.receivedData=[obj.receivedData;received(1:obj.packetLen)];
                    i=i+1;
                    window=window-1;
                    if window == 0 
                        window=5;
                    end
                end
            end
            
            obj.receivedData=obj.receivedData(1:length(obj.data));
            
        end
    end
end