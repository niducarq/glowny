classdef SelectiveRepeat
properties
        receivedData;
        retransmitions;
        sentPackets;
        
        errg;
        errb;
        gtb;
        btg;
        
        dataLen;
        data;
        frameLen;
        checksumType;
        channelType;
        
    end
    methods
        function obj = SelectiveRepeat(data, frameLen, checksumType, channelType, errg,errb,gtb,btg)
           obj.receivedData=int16.empty;
           obj.retransmitions=0;
           obj.sentPackets=0;
           obj.data= data;
           obj.frameLen=frameLen;
           obj.checksumType=checksumType;
           obj.channelType=channelType;
           
           
           obj.errg=errg;
           obj.errb=errb;
           obj.gtb=gtb;
           obj.btg=btg;
           
        end
        
        function war = getretransmitions(obj)
            war = obj.retransmitions;
        end
        
        function war = getsentPackets(obj)
            war = obj.sentPackets;
        end
        
        function war = getdata(obj)
            war = obj.data;
        end
        
        function war = getreceivedData(obj)
            war = obj.receivedData;
        end
        
        
        
        function obj = SelectiveRepeat_run(obj)
            % Funkcja przeprowadzaj?ca symulacj? ARQ typu Selective repeat.
            %   Argumenty:
            %       data: dane do przes?ania
            %       frameLen: dlugosc ramki
            %       checksumType: typ kontroli poprawno?ci
            %       channelType: typ kana?u
            %   Wynik:
            %       receivedData: dane odebrane
            %       retransmissions: przeprowadzone retransmisje
            if(mod(length(obj.data),obj.frameLen )~=0)
                message= [obj.data; zeros(obj.frameLen-mod(length(obj.data),obj.frameLen),1)];
            else
                message=obj.data;
            end
            
            segments=reshape(message, obj.frameLen,[]);
            frameArchCount = numel(segments(1, :));
            
          %  disp('SEGMENTS: ')
          %  disp(mat2str(segments));
            frameCount = 1;
          
            frame = zeros(numel(checksum(segments(:,1), obj.checksumType) ) + obj.frameLen, frameArchCount)
            
            i = 1;
            
            while(i < frameArchCount + 1) 
               toadd = [segments(:,i); checksum(segments(:,i),obj.checksumType) ]
               frame = [ frame(:, 1:end) toadd] 
               
               frame =  frame(:, 2:end)
                i = i + 1;
            end
            
           
            boolTab = 1:frameArchCount;
            
            segmentsCount = 1;
           %  disp(num2str(size(segments,2)));
            
            while (segmentsCount <= numel(segments(1, :)))    
                %   disp('PETLA1: ')
                   num2str(segmentsCount);
                   
                    i=1;
                 while i  <= frameArchCount
                  %  disp('PETLA2: ')
                    
                 %   if( i > numel(segments(1, :)))
                  %    break;  
                   % end
                    canal = TransmissionChannel(frame(:,i),obj.errg,obj.errb,obj.gtb,obj.btg);
                    received=GenerateNoise(canal);
                    obj.sentPackets=obj.sentPackets+1;
                    if (~isequal(received(obj.frameLen+1:end),checksum(received(1:obj.frameLen), obj.checksumType)))
                         obj.retransmitions=obj.retransmitions+1;   %zwiekszenie licznika
                         boolTab(:,i) = 0;
                       %   disp(mat2str(boolTab));
                          i = i + 1;
                          j = 1;
                            toadd4 = [];
                           while j <= obj.frameLen
                              toadd4 = [toadd4; 3]
                              j = j + 1;
                           end
                           
                         obj.receivedData = [obj.receivedData ; toadd4 ];
                    
                   else
                               % APPEND 
                           obj.receivedData = [obj.receivedData ; received(1:obj.frameLen) ] % DOŁĄCZENIE POPRAWNIE WYSŁANYCH BITÓW DO ODEBRANYCH
                         %  disp('RECEIVED DATA: ')
                        %   disp(mat2str(obj.receivedData))
                          boolTab(:,i) = 1;
                          i = i + 1;
                        %   disp(mat2str(boolTab));
                    end
                 end 
                 
              i = 1 ; 
              
              while i <= obj.frameLen
                     if (segmentsCount > frameArchCount)
                         break;
                     end
                     
                if(isequal(boolTab(:, 1), 0))
                     
                    canal = TransmissionChannel(frame(:,i),obj.errg,obj.errb,obj.gtb,obj.btg);
                    received=GenerateNoise(canal);
                    obj.sentPackets=obj.sentPackets+1;
                    
                      if (~isequal(received(obj.frameLen+1:end),checksum(received(1:obj.frameLen), obj.checksumType)))
                         obj.retransmitions=obj.retransmitions+1;   %zwiekszenie licznika
                         boolTab(:,1) = 0;
                        %  disp(mat2str(boolTab));
                      else
                       genData = obj.receivedData;
                       
                       
                       genData( (obj.frameLen * (segmentsCount - 1)) + 1  :(obj.frameLen * segmentsCount)) = received(1:obj.frameLen);
                               % APPEND 
                           obj.receivedData = genData; % DOŁĄCZENIE POPRAWNIE WYSŁANYCH BITÓW DO ODEBRANYCH
                        %   disp('RECEIVED DATA: ')
                         %  disp(mat2str(obj.receivedData))
                          %boolTab(:,i) = 1;
                          
                         
                         
                         if(frameCount+frameArchCount > numel(segments(1, :)))
                              nextSegment = zeros(numel(checksum(segments(:,1), obj.checksumType) ) + obj.frameLen, frameArchCount);     
                         else  
                                nextSegment = [segments(:,frameCount);checksum(segments(:,frameCount),obj.checksumType)];
                         end
                         
                    frame(:,1) = [];
                    
                    frame = [ frame nextSegment ];
                    frameCount = frameCount + 1;
                    segmentsCount = segmentsCount + 1;
                    boolTab(:,1) = []; 
                    boolTab = [boolTab(1:end), 0];
           
                    
                          i = i + 1;
                          % disp(mat2str(boolTab));
                    end
                    
                end
                
                 if(isequal(boolTab(:, 1), 1))

                         if(frameCount+frameArchCount > numel(segments(1, :)))
                              nextSegment = zeros(numel(checksum(segments(:,1), obj.checksumType) ) + obj.frameLen, frameArchCount);        
                         else  
                                nextSegment = [segments(:,frameCount);checksum(segments(:,frameCount),obj.checksumType)];
                         end
                    frame(:,1) = [];
                    frameCount = frameCount + 1;
                    frame = [ frame nextSegment ];
                    segmentsCount = segmentsCount + 1;
                    boolTab(:,1) = []; 
                    boolTab = [boolTab(1:end), 0];
                    i = i + 1;
                 end
                 
                
              end
              
              if(numel(obj.data) <= numel(obj.receivedData))
              return;
              end
              
            end 
         end
      end
end

            
