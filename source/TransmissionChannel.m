classdef TransmissionChannel
    properties
        Vec;
        Error_rate_good;
        Error_rate_bad;
        Good_to_bad;
        Bad_to_good;
        State;
    end
    methods
        function obj = TransmissionChannel(vec, errg,errb,gtb,btg)
            obj.Vec = vec;
            obj.Error_rate_good = errg;
            obj.Error_rate_bad = errb;
            obj.Good_to_bad = gtb;
            obj.Bad_to_good = btg;
            obj.State=0;
        end
        
        function r = GenerateNoise(obj)
                            
           %{
            for i=1:numel(obj.Vec)
              if rand() <= obj.Error_rate
               if obj.Vec(i,1) == 0
               obj.Vec(i,1) = 1;
               else
               obj.Vec(i,1) = 0; 
               end
              end
           end 
          %}
            for i=1:numel(obj.Vec)
                if obj.State==0
                    if rand() <= obj.Error_rate_good
                        obj.Vec(i,1)=negate(obj.Vec(i,1));
                    end
                    if rand() < obj.Good_to_bad
                        obj.State = negate(obj.State);
                    end
                else
                    if rand() <= obj.Error_rate_bad
                        obj.Vec(i,1)=negate(obj.Vec(i,1));
                    end
                    if rand() < obj.Bad_to_good
                        obj.State = negate(obj.State);
                    end
                end
            end
            
            r = obj.Vec;
        end
    end
end

function out = negate(in)
    if in==0
        out=1;
    else
        out=0;
    end
end

